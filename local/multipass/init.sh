#!/usr/bin/env bash

set -e
set -o pipefail

SCRIPT_DIR=`dirname "$0"`
VIRT_HOST_NAME="${VIRT_HOST_NAME:-meetup}"

multipass launch -n "${VIRT_HOST_NAME}" --cloud-init "${SCRIPT_DIR}/init.yaml"

multipass info "${VIRT_HOST_NAME}"
