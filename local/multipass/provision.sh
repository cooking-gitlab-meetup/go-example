#!/usr/bin/env bash

set -e
set -o pipefail

VIRT_HOST_NAME="${VIRT_HOST_NAME:-meetup}"
SCRIPT_PATH=$(dirname $0)

IP=$(multipass list | grep "${VIRT_HOST_NAME}" | grep -i RUNNING | tr -s ' ' | cut -f3 -d' ')

ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook  -e "ansible_host=${IP}" -e "ansible_port=22" $@