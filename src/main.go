package main

import (
	"fmt"
	"log"
	"net/http"
)

func main() {
	if err := runapp(); err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}

func runapp() error {
	http.HandleFunc("/", handleError(handler))

	return http.ListenAndServe(":80", nil)
}

func handler(w http.ResponseWriter, r *http.Request) error {
	if err := r.ParseForm(); err != nil {
		return err
	}

	_, err := fmt.Fprintf(w, "Hello, %s", r.Form.Get("name"))
	return err
}

func handleError(h func(w http.ResponseWriter, r *http.Request) error) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		err := h(w, r)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			_, _ = fmt.Fprintf(w, "Internal error")
		}
	}
}