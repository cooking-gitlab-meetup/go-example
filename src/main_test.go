package main

import (
	"errors"
	"github.com/go-redis/redis/v7"
	"github.com/stretchr/testify/require"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/httptest"
	"net/url"
	"os"
	"testing"
)

func TestHello(t *testing.T) {
	server := httptest.NewServer(handleError(handler))
	defer server.Close()

	r := require.New(t)

	resp, err := http.PostForm(server.URL, url.Values{
		"name": {"Bob"},
	})

	r.NoError(err)

	defer func () {
		if err := resp.Body.Close(); err != nil {
			log.Fatal(err)
		}
	}()

	body, err := ioutil.ReadAll(resp.Body)

	r.NoError(err)
	r.Equal("Hello, Bob", string(body))
}

func TestError(t *testing.T) {
	server := httptest.NewServer(handleError(func (w http.ResponseWriter, r *http.Request) error {
		return errors.New("test")
	}))
	defer server.Close()

	r := require.New(t)

	resp, err := http.PostForm(server.URL, url.Values{
		"name": {"Bob"},
	})

	r.NoError(err)

	defer func () {
		if err := resp.Body.Close(); err != nil {
			log.Fatal(err)
		}
	}()

	body, err := ioutil.ReadAll(resp.Body)

	r.NoError(err)
	r.Equal(http.StatusInternalServerError, resp.StatusCode)
	r.Equal("Internal error", string(body))
}

func TestRedis(t *testing.T) {
	redisHost := os.Getenv("REDIS")
	if redisHost == "" {
		redisHost = "127.0.0.1:6379"
	}

	client := redis.NewClient(&redis.Options{
		Addr:     redisHost,
		Password: "", 
		DB:       0,  
	})

	_, err := client.Ping().Result()
	r := require.New(t)

	r.NoError(err)
}