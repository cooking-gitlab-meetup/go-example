## Local installation


        $ local/multipass/init.sh
        $ local/multipass/provision.sh -i hosts deploy/setup.yml -e"ansible_user=root"
        $ local/multipass/provision.sh -i hosts deploy/deploy.yml -e"ansible_user=root"
        $ curl "http://$(multipass list | grep meetup | grep -i RUNNING | tr -s ' ' | cut -f3 -d' ')" -d"name=Test" 
